# react-signup-verification-boilerplate

React - Email Sign Up with Verification, Authentication & Forgot Password

For documentation and a live demo see https://jasonwatmore.com/post/2020/04/22/react-email-sign-up-with-verification-authentication-forgot-password

## React Boilerplate App Overview
The React boilerplate app runs with a fake backend by default to enable it to run completely in the browser without a real backend api (backend-less), to switch to a real backend api you just have to remove a couple of lines of code from the main react entry file (`/src/index.jsx`). 
You can build your own api or hook it up with the Node.js + MongoDB or ASP.NET Core 3.1 boilerplate api available (instructions below).

There are no users registered in the application by default, in order to login you must first register and verify an account. 
The fake backend displays "email" messages on screen because it can't send real emails, so after registration a "verification email" is displayed with a link to verify the account just registered, click the link to verify the account and login to the app.

The first user registered is assigned to the `Admin` role and subsequent users are assigned to the regular `User` role. 
Admins can access the admin section and manage all users, while regular users can only update their own profile.

### JWT Authentication with refresh tokens
Authentication is implemented with JWT access tokens and refresh tokens. 
On successful authentication the api (or fake backend) returns a short-lived JWT access token that expires after 15 minutes, and a refresh token that expires after 7 days in a cookie. 
The JWT is used for accessing secure routes on the api and the refresh token is used for generating new JWT access tokens when (or just before) they expire, the React app starts a timer to refresh the JWT token 1 minute before it expires to keep the user logged in.

## Run the React Boilerplate App Locally
1. Install NodeJS and NPM from https://nodejs.org
1. Download or clone the project source code from https://github.com/cornflourblue/react-signup-verification-boilerplate
1. Install all required npm packages by running `npm install` from the command line in the project root folder (where the package.json is located).
1. Start the application by running `npm start` from the command line in the project root folder, this will launch a browser displaying the application.

For more info on setting up a React development environment see React - Setup Development Environment.

## Run the React App with a Boilerplate Node.js + MongoDB API
For full details about the boilerplate Node + Mongo api see [Node + Mongo - Boilerplate API with Email Sign Up, Verification, Authentication & Forgot Password](https://jasonwatmore.com/post/2020/05/13/node-mongo-api-with-email-sign-up-verification-authentication-forgot-password). 
But to get up and running quickly just follow the below steps.

1. Install MongoDB Community Server from  https://www.mongodb.com/download-center/community.
1. Run MongoDB, instructions are available on the install page for each OS at https://docs.mongodb.com/manual/administration/install-community/
1. Download or clone the project source code from https://github.com/cornflourblue/node-mongo-signup-verification-api
1. Install all required npm packages by running `npm install` or `npm i` from the command line in the project root folder (where the package.json is located).
1. Configure SMTP settings for email within the `smtpOptions` property in the `/src/config.json` file. For testing you can create a free account in one click at https://ethereal.email/ and copy the options below the title Nodemailer configuration.
1. Start the api by running `npm start` from the command line in the project root folder, you should see the message `Server listening on port 4000`.
1. Back in the React app, remove or comment out the 2 lines below the comment `// setup fake backend` located in the `/src/index.jsx` file, then start the React app and it should now be hooked up with the Node + Mongo API.

## React Boilerplate Project Structure
All source code for the React boilerplate app is located in the `/src` folder. 
Inside the src folder there is a folder per feature (account, admin, app, home, profile) as well as folders for non-feature code that can be shared across different parts of the app (_components, _helpers, _services).

I prefixed non-feature folders with an underscore `_` to group them together and make it easy to distinguish between features and non-features, it also keeps the project folder structure shallow, so it's quick to see everything at a glance from the top level and to navigate around the project.

The `index.js` files in each non-feature folder re-export all the modules from the folder, so they can be imported using only the folder path instead of the full path to each file, and to enable importing multiple modules in a single import (e.g. `import { accountService, alertService } from '@/_services'`).

I named the root component file in each feature folder `Index.jsx` so it can be imported using only the folder path (e.g. `import { App } from './app';`), removing the need for an extra `index.js` file that re-exports the component.

## Formik Forms
All forms in the React boilerplate app are built with the `Formik` component. 
The initial values of each field are set in the `initialValues` property, validation rules and error messages are set in the `validationSchema` property, the `onSubmit` function gets called when the form is submitted and valid, and the JSX template for each form is returned by the callback function contained within the `<Formik>...</Formik>` component tag. 
For more info see the [Formik docs](https://jaredpalmer.com/formik/docs/overview).

## Components
### Alert Component
*Path: `src/_components/Alert.jsx`*

The alert component controls the adding & removing of bootstrap alerts in the UI, it maintains an array of `alerts` that are rendered in the template returned by the React Hooks function component.

The `useEffect()` hook is used to subscribe to the observable returned from the `alertService.onAlert()` method, this enables the alert component to be notified whenever an alert message is sent to the alert service and add it to the `alerts` array for display. 
Sending an alert with an empty message to the alert service tells the alert component to clear the alerts array. 
The `useEffect()` hook is also used to register a route change listener by calling `history.listen()` which automatically clears alerts on route changes.

The empty dependency array `[]` passed as a second parameter to the `useEffect()` hook causes the react hook to only run once when the component mounts, similar to the `componentDidMount()` method in a traditional react class component. 
The function returned from the `useEffect()` hook cleans up the subscribtions when the component unmounts, similar to the `componentWillUnmount()` method in a traditional react class component.

The `removeAlert()` function removes the specified alert object from the array, it allows individual alerts to be closed in the UI.

The `cssClasses()` function returns a corresponding bootstrap alert class for each of the alert types, if you're using something other than bootstrap you could change the CSS classes returned to suit your application.

The returned JSX template renders a bootstrap alert message for each alert in the alerts array.

For more info see [React Hooks + Bootstrap - Alert Notifications](https://jasonwatmore.com/post/2020/04/11/react-hooks-bootstrap-alert-notifications).

### Nav Component
*Path: `src/_components/Nav.jsx`*

The nav component displays the primary and secondary navigation bars in the example. The component subscribes to the `accountService.user` observable and only displays the nav if the user is logged in.

Only the admin section has a secondary nav which contains a link to the `/admin/users` subsection. The `AdminNav` component is only displayed in the admin section by using the React router `Route` component and setting the path to `"/admin"` (`<Route path="/admin" component={AdminNav} />`).

The React router `NavLink` component automatically adds the `active` class to the active nav item, so it is highlighted in the UI.

### Private Route Component
*Path: `src/_components/PrivateRoute.jsx`*

The React private route component renders a route component if the user is logged in and in an authorized role for the route, if the user isn't logged in they're redirected to the `/login` page, if the user is logged in but not in an authorized role they're redirected to the home page.

## Helpers
### Fake Backend
*Path: `src/_helpers/fake-backend.js`*

The fake backend is enabled by executing the below `configureFakeBackend()` function which monkey patches fetch() to create a custom fetch function.

The new custom fetch function returns a javascript `Promise` that resolves after a half second delay to simulate a real api call. 
The fake backend is organized into a top level `handleRoute()` function that checks the request url and method to determine how the request should be handled. 
For intercepted routes one of the below `// route functions` is called, for all other routes the request is passed through to the real backend via the realFetch() function which points to the original `window.fetch` function. 
Below the route functions there are `// helper functions` for returning different response types and performing small tasks.

The fake backend can't send emails so instead displays "email" messages on the screen by calling `alertService.info()` with the contents of the email e.g. "verify email" and "reset password" emails.

For more info see [React + Fetch - Fake Backend Example for Backendless Development](https://jasonwatmore.com/post/2020/03/10/react-fetch-fake-backend-example-for-backendless-development).

### Fetch Wrapper
*Path: `src/_helpers/fetch-wrapper.js`*

The fetch wrapper is a lightweight wrapper around the native browser `fetch()` function used to simplify the code for making HTTP requests. 
It contains methods for `get`, `post`, `put` and `delete` requests, it automatically handles the parsing of JSON data from responses, and throws an error if the HTTP response is not successful (`!response.ok`). 
If the response is `401 Unauthorized` or `403 Forbidden` the user is automatically logged out.

The `authHeader()` function is used to automatically add a JWT auth token to the HTTP Authorization header of the request if the user is logged in, and the request is to the application api url.

With the fetch wrapper a `POST` request can be made as simply as: `fetchWrapper.post(url, body);`. 
It is used in the example app by the account service.

### History
*Path: `src/_helpers/history.js`*

The history helper creates the browser history object used by the React app, it is passed to the `Router` component in the main react entry file and enables us to access the history object outside react components, for example from the `logout()` method of the account service.

### Role
*Path: `src/_helpers/role.js`*

The role object defines the all the roles in the example application, I created it to use like an enum to avoid passing roles around as strings, so instead of `'Admin'` we can use `Role.Admin`.

## Services

### Account Service

*Path: `src/_services/account.service.js`*

The account service handles communication between the React app, and the backend api for everything related to accounts. 
It contains methods for the sign up, verification, authentication, refresh token and forgot password flows, as well as standard CRUD methods for retrieving and modifying user data. 
All HTTP requests to the api are made by calling the fetch wrapper.

On a successful login the api returns the user details, and a JWT token which are published to all subscribers with the call to `userSubject.next(user)`, the api also returns a refresh token cookie which is stored by the browser. 
The method then starts a countdown timer by calling `startRefreshTokenTimer()` to auto refresh the JWT token in the background (silent refresh) one minute before it expires in order to keep the user logged in.

The `logout()` method makes a POST request to the API to revoke the refresh token that is stored in the `refreshToken` cookie in the browser, then cancels the silent refresh running in the background by calling `stopRefreshTokenTimer()`, then logs the user out by publishing a null value to all subscriber components (`userSubject.next(null)`), and finally redirects the user to the login page.

The `user` property exposes an RxJS observable (`userSubject.asObservable()`) so any component can subscribe to be notified when a user logs in, logs out, has their token refreshed or updates their profile. 
The notification is triggered by the call to `userSubject.next()` from each of the corresponding methods in the service. 
For more info about using React with RxJS see [React Hooks + RxJS - Communicating Between Components with Observable & Subject](https://jasonwatmore.com/post/2020/04/21/react-hooks-rxjs-communicating-between-components-with-observable-subject).

### Alert Service
*Path: `src/_services/alert.service.js`*

The alert service acts as the bridge between any component in the React application, and the alert component that actually displays the alert notification. 
It contains methods for sending, clearing and subscribing to alerts.

The `AlertType` object defines the types of alerts supported by the application.

You can trigger alert notifications from any component or service by calling one of the convenience methods for displaying the different types of alerts: `success()`, `error()`, `info()` and `warn()`.

#### Alert convenience method parameters
* The first parameter is the alert `message` string, which can be plain text or HTML
* The second parameter is an optional `options` object that supports an `autoClose` boolean property and `keepAfterRouteChange` boolean property:
    * `autoClose` - if true tells the alert component to automatically close the alert after three seconds. Default is true.
    * `keepAfterRouteChange` - if true prevents the alert from being closed after one route change, this is handy for displaying messages after a redirect such as when a new user is created or updated. Default is false.

For more info see [React Hooks + Bootstrap - Alert Notifications](https://jasonwatmore.com/post/2020/04/11/react-hooks-bootstrap-alert-notifications).

## Account Features
### Forgot Password Component
*Path: `src/account/ForgotPassword.jsx`*

The forgot password component contains a form built with the `Formik` library with a single field for entering the email of the account for which you have forgotten the password.

On submit the component calls `accountService.forgotPassword(email)` and displays either a success or error message. 
If the email matches a registered account the fake backend displays a password reset "email" with instructions in the UI below the success message (A real backend api would send an actual email for this step), the instructions include a link to reset the password of the account.


### Account Index Component
*Path: `src/account/Index.jsx`*

The `Account` component is the root component of the account section/feature, it defines routes for each of the pages within the account section which handle all of the authentication and related functionality.

A `useEffect` hook is used check if the user is already logged in when they try to access an accounts page, so they can be automatically redirected to the home page (`'/'`), since authenticated users have no use for any of the accounts pages.

As a convention I named the root component file in each feature folder `Index.jsx`, so it can be imported using only the folder path (`import { Account } from '@/account';`), removing the need for an extra `index.js` file that re-exports the Account component.

### Login Component
*Path: `src/account/Login.jsx`*

The login component contains a pretty standard login form built with the `Formik` library that contains fields for `email` and `password`.

On a successful login the user is redirected to the page they were trying to access before logging in or to the home page (`"/"`) by default. 
The `from` path is added to the `location.state` when redirected by the private route component. 
On failed login the error returned from the backend is displayed in the UI.

### Register Component
*Path: `src/account/Register.jsx`*

The register component contains an account registration form with fields for title, first name, last name, email, password, confirm password, and an accept Ts & Cs checkbox. 
All fields are required including the checkbox, the email field must be a valid email address, the password field must have a min length of 6 and must match the confirm password field.

On successful registration a success message is displayed, and the user is redirected to the login page, then the fake backend displays a verification "email" with instructions in the UI below the success message (A real backend api would send an actual email for this step), the instructions include a link to verify the account.

### Reset Password Component
*Path: `src/account/ResetPassword.jsx`*

The reset password component displays a form for resetting an account password when it receives a valid password reset `token` in the url querystring parameters. 
The token is validated when the component mounts by calling `accountService.validateResetToken(token)` from inside a `useEffect()` react hook function, the empty dependency array passed to the react hook makes it run only once when the component mounts.

The `tokenStatus` controls what is rendered on the page, the initial status is `Validating` before changing to either `Valid` or `Invalid`. The `TokenStatus` object/enum is used to set the status, so we don't have to use string values.

On form submit the password is reset by calling `accountService.resetPassword(token, password)` which sends the token and new password to the backend. 
The backend should validate the token again before updating the password, see the `resetPassword()` function in the fake backend for an example.

On successful password reset the user is redirected to the login page with a success message and can log in with the new password.

### Verify Email Component
*Path: `src/account/VerifyEmail.jsx`*

The verify email component is used to verify new accounts before they can log in to the boilerplate app. 
When a new account is registered an email is sent to the user containing a link back to this component with a verification `token` in the querystring parameters. 
The token from the email link is verified when the component mounts by calling `accountService.verifyEmail(token)` from inside a `useEffect()` react hook function, the empty dependency array passed to the React hook makes it run only once when the component mounts.

On successful verification the user is redirected to the login page with a success message and can log in to the account, if token verification fails an error message is displayed, and a link to the forgot password page which can also be used to verify an account.

NOTE: When using the app with the fake backend the verification "email" is displayed on the screen when a new account is registered.

## Admin Feature
### Users Add/Edit Component
*Path: `src/admin/users/AddEdit.jsx`*

The users add/edit component is used for both adding and editing users, the form is in "add mode" when there is no user id route parameter (`match.params.id`), otherwise it is in "edit mode". The variable `isAddMode` is used to change the form behaviour based on which mode it is in, for example in "add mode" the password field is required, and in "edit mode" (`!isAddMode`) the account service is called when the component mounts to get the user details (`accountService.getById(id)`) to preset the field values.

On submit a user is either created or updated by calling the account service, and on success you are redirected back to the users list page with a success message.

### Users Index Component
*Path: `src/admin/users/Index.jsx`*

The `Users` component is the root component of the users section/feature, it defines routes for each of the pages within the users section.

The first route matches the root users path (`/admin/users`) making it the default route for this section, so by default the users `List` component is displayed.
The second and third routes are for adding and editing users, they match different routes but both load the same component (AddEdit), and the component modifies its behaviour based on the route.

As a convention I named the root component file in each feature folder `Index.jsx` so it can be imported using only the folder path (`import { Users } from './users';`), removing the need for an extra `index.js` file that re-exports the `Users` component.

### Users List Component
*Path: `src/admin/users/List.jsx`*

The users list component displays a list of all users and contains buttons for adding, editing and deleting users. 
A `useEffect` hook is used to get all users from the account service and store them in the component state by calling `setUsers()`.

The delete button calls the `deleteUser()` function which first updates the user in component state with an `isDeleting = true` property so the UI displays a spinner on the delete button, it then calls `accountService.delete()` to delete the user and removes the deleted user from component state so it is removed from the UI.

### Admin Index Component
*Path: `src/admin/Index.jsx`*

The `Admin` component is the root component of the admin section/feature, it defines routes for each of the pages within the admin section. 
The admin section is only accessible to admin users, access is controlled by a private route in the app component.

The first route matches the root admin path (`/admin`) making it the default route for this section, so by default the admin overview component is displayed, and the second route matches the `/admin/users` path to the users component for managing all users in the system.

As a convention I named the root component file in each feature folder `Index.jsx` so it can be imported using only the folder path (`import { Admin } from './admin';`), removing the need for an extra `index.js` file that re-exports the Admin component.

### Admin Overview Component
*Path: `src/admin/Overview.jsx`*

The admin overview component displays some basic HTML and a link to the user admin section.

## App Feature
### App Index Component
*Path: `src/app/Index.jsx`*

The `App` component is the root component of the example app, it contains the outer html, main nav, global alert, and top level routes for the application.

As a convention I named the root component file in each feature folder `Index.jsx` so it can be imported using only the folder path (`import { App } from './app';`), removing the need for an extra `index.js` file that re-exports the App component.

The first route (`<Redirect from="/:url*(/+)" to={pathname.slice(0, -1)} />`) automatically removes trailing slashes from URLs which can cause issues and are a side-effect of using relative react router links (e.g. `<Link to=".">`). 
For more info see [React Router - Relative Links Example](https://jasonwatmore.com/post/2020/03/26/react-router-relative-links-example).

The last route (`<Redirect from="*" to="/" />`) is a catch-all redirect route that redirects any unmatched paths to the home page.

All routes are restricted to authenticated users except for the account section, and the admin section is restricted to users in the `Admin` role. 
The private route component (`PrivateRoute`) is used to restrict access to routes.

## Home Feature
### Home Index Component
*Path: `src/home/Index.jsx`*

The `Home` component is a simple react function component that displays some HTML with the first name of the logged in user.

As a convention I named the root component file in each feature folder `Index.jsx` so it can be imported using only the folder path (`import { Home } from '@/home';`), removing the need for an extra `index.js` file that re-exports the Home component.

## Profile Feature
### Profile Details Component
*Path: `src/profile/Details.jsx`*

The profile details component displays the name and email of the authenticated user with a link to the update profile page.

### Profile Index Component
*Path: `src/profile/Index.jsx`*

The `Profile` component is the root component of the profile section/feature, it defines routes for each of the pages within the profile section. 
The profile section is accessible to all authenticated users, access is controlled by a private route in the app component.

The first route matches the root profile path (`/profile`) making it the default route for this section, so by default the profile details component is displayed, and the second route matches the `/profile/update` path to the profile update component.

As a convention I named the root component file in each feature folder `Index.jsx` so it can be imported using only the folder path (`import { Profile } from './profile';`), removing the need for an extra `index.js` file that re-exports the Profile component.

### Profile Update Component
*Path: `src/profile/Update.jsx`*

The profile update component enables the current user to update their profile, change their password, or delete their account. 
It sets the `initialValues` of the fields with the current user from the account service (`accountService.userValue`).

On successful update the user is redirected back to the profile details page with a success message. 
On successful delete the user is logged out and a message is displayed.

## Root Source
### Base Index HTML File
*Path: `src/index.html`*

The base index html file contains the outer html for the whole react boilerplate application. 
When the app is started with `npm start`, webpack bundles up all of the react code into a single javascript file and injects it into the body of this page to be loaded by the browser. 
The react app is then rendered in the `<div id="app"></div>` element by the main react entry file below.

### Main React Entry File
*Path: `src/index.jsx`*

The root `index.jsx` file bootstraps the react boilerplate app by rendering the `App` component (wrapped in a react `Router` component) into the app div element defined in the base index html file above.

Before the app starts up an attempt is made to automatically authenticate by calling `accountService.refreshToken()` to get a new JWT token from the api. 
If the user has logged in previously (without logging out) and the browser still contains a valid refresh token cookie then they will be automatically logged in when the app loads.

The `history` helper is passed to the `Router` instead of using the `BrowserRouter` component (which comes with the history built in) so we can access the history object outside of react components, for example from the `logout()` method of the account service.

The boilerplate application uses a fake backend that stores data in browser local storage to mimic a real api, to switch to a real backend simply remove the 2 lines of code below the comment `// setup fake backend`.

### Global LESS/CSS Styles
*Path: `src/styles.less`*

The styles.less file contains global custom LESS/CSS styles for the react boilerplate app. For more info see [React - How to add Global CSS / LESS styles to React with webpack](https://jasonwatmore.com/post/2020/02/10/react-how-to-add-global-css-less-styles-to-react-with-webpack).

# Project Files
### Babel RC (Run Commands) File
*Path: `.babelrc`*

The babel config file defines the presets used by babel to transpile the React and ES6 code. 
The babel transpiler is run by webpack via the babel-loader module configured in the `webpack.config.js` file below.

### Package.json
*Path: `package.json`*

The `package.json` file contains project configuration information including package dependencies which get installed when you run npm install. 
Full documentation is available on the [npm docs website](https://docs.npmjs.com/files/package.json).

### Webpack Config
*Path: `webpack.config.js`*

Webpack is used to compile and bundle all the project files so they're ready to be loaded into a browser, it does this with the help of loaders and plugins that are configured in the `webpack.config.js` file. 
For more info about webpack check out the [webpack docs](https://webpack.js.org/concepts/).

The webpack config file also defines a global config object for the application using the externals property, you can also use this to define different config variables for your development and production environments.








