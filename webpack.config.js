const webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const dotenv = require('dotenv-defaults');

const path = require('path');
const env = dotenv.config().parsed;

let envKeys = {};
try {
    envKeys = Object.keys(env).reduce((prev, next) => {
        prev[`process.env.${next}`] = JSON.stringify(env[next]);
        return prev;
    }, {});
} catch (e) {
    console.warn("No .env file provided.");
}

module.exports = {
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader'
            },
            {
                test: /\.less$/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'},
                    {loader: 'less-loader'}
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'}
                ]
            }
        ]
    },
    resolve: {
        mainFiles: ['index', 'Index'],
        extensions: ['.js', '.jsx'],
        alias: {
            '@': path.resolve(__dirname, 'src/'),
        }
    },
    plugins: [
        new HtmlWebpackPlugin({template: './src/index.html'}),
        new webpack.DefinePlugin(envKeys)
    ],
    devServer: {
        historyApiFallback: true
    }
}